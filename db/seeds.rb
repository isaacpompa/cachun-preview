# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(first_name: 'Isaac S', last_name: 'Pompa', carrer: 'Ciencias de la Computación', biography: 'Soy estudiante de Ciencias de la Computación', member: true, email: 'isaacboolean@yimail.com')
User.create!(first_name: 'Manuel', last_name: 'Manu', carrer: 'Matemáticas', biography: 'Soy estudiante de Matemáticas', member: true, email: 'manuel@yimail.com')
User.create!(first_name: 'Carlos', last_name: 'Prieto', carrer: 'Física', biography: 'Soy estudiante de Física', member: true, email: 'carlosfisica@mail.com')
User.create!(first_name: 'Lalo', last_name: 'Su_apellido', carrer: 'Actuaría', biography: 'Soy estudiante de Actuaría', member: true, email: 'lalolean@yimail.com')